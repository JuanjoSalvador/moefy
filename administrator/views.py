from django.shortcuts import render, redirect

def index(request):
    if request.user.is_staff:
        return render(request, 'administrator/base.html')
    else:
        return redirect('/')

def test(request):
    return render(request, 'administrator/test.html')