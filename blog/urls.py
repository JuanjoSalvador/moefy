from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^$', views.post_list, name='home'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^submit/$', views.submit, name='submit'),
    url(r'^users/(?P<username>\w{0,50})/$', views.profile, name='profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)