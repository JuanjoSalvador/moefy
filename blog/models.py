from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title  = models.CharField(max_length = 200)
    text   = models.TextField(blank = True, null = True)

    created_date = models.DateTimeField(default = timezone.now())
    published_date = models.DateTimeField(blank = True, null = True)

    image = models.ImageField(upload_to = '')

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Profile(models.Model):
    user    = models.OneToOneField(User, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to = '', blank=True, null=True)
    twitter = models.CharField(max_length = 100, blank=True, null=True)
    bio     = models.TextField(blank=True, null=True)
    age     = models.IntegerField(blank=True, null=True)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return self.user.username
